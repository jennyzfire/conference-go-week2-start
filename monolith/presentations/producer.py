import pika
import json


def approve_or_reject_presentation(
    presenter_name, presenter_email, title, status_queue
):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    if status_queue == "presentation_rejections":
        channel.queue_declare(queue="presentation_rejections")
    else:
        channel.queue_declare(queue="presentation_approvals")

    channel.basic_publish(
        exchange="",
        routing_key=status_queue,
        body=json.dumps(
            {
                "presenter_name": presenter_name,
                "presenter_email": presenter_email,
                "title": title,
            }
        ),
    )
    # print(
    #     json.dumps(
    #         {
    #             "presenter_name": presenter_name,
    #             "presenter_email": presenter_email,
    #             "title": title,
    #         }
    #     )
    # )
    connection.close()
